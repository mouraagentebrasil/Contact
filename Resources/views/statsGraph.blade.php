<html>
<header>
    <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/dt-1.10.9/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/r/dt/dt-1.10.9/datatables.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
</header>
<body>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<table id="datatable" style="display: none;">
    <thead>
    <tr>
        <th>Total EMAIL</th>
        <th>Total EMAIL com erro</th>
        <th>Total SMS</th>
        <th>Total SMS com erro</th>
    </tr>
    </thead>
</table>

<script>
    var montaInfo = function(data){
        var html = '';
        //listando sms
        for(i=0;i<data.sms.length;i++){
            html += 'Data: '+data.sms[i].data_op+' <br> Total de SMS:'+data.sms[i].total_sms+'<br>';
            for(k=0;k<data.sms_erro.length;k++){
                if(data.sms[i].data_op == data.sms_erro[k].data_op){
                    html+= 'Total SMS com Falha: '+data.sms_erro[k].total_sms_erro+"<br>";
                }
            }

            html+='--------------------------------<br>';
        }
        for(i=0;i<data.email.length;i++){
            html += 'Data: '+data.email[i].data_op+'<br> Total de EMAIL:'+data.email[i].total_email+'<br>';
            for(k=0;k<data.email_erro.length;k++){
                if(data.email[i].data_op == data.email_erro[k].data_op){
                    html+= 'Total de Email com Falha: '+data.email_erro[k].total_email_erro+"<br>";
                }
            }
            html+='--------------------------------<br>';
        }

        $('#infoStats').html(html);
    };

    var montaLogicTable = function(data){
        var html = '';
        html += '<tbody>';
        if(data.email.length > data.sms.length){
            for(ems=0;ems<data.email.length;ems++){
                console.log(data.email[ems]);
                console.log(data.sms[ems]);
                html+= '<tr>';
                html+= ' <th></th>';
                html+= ' <td>Total SMS</td>';
                html+= ' <td>Total SMS com erro</td>';
                html+= ' <td>Total EMAIL</td>';
                html+= ' <th>Total EMAIL com erro</th>';
                html+= ' </tr>';
                html+= '<tr>';
                //newData.push({data.email[ems].data_op:[{"date":data.email[ems].data_op}]});
                html += ' <td>'+data.email[ems].data_op+'</td>';
                for(em=0;em<data.email[ems].length;em++){
                    //html += ' <td>'+data.email[em].data_op+'</td>';
                    if(data.email_erro[em] !== undefined &&
                        data.email[ems].data_op == data.email_erro[em].data_op) {
                        html+= ' <td>'+data.email[em].total_email+'</td>';
                        html+= ' <td>'+data.email_erro[em].total_email_erro+'</td>';
                    }
                }
                for(esm=0;esm<data.sms[ems].length;esm++){
                    if(data.sms_erro[es]!==undefined &&
                        data.sms[ems].data_op == data.sms_erro[esm].data_op) {
                        html+= ' <td>'+data.sms[em].total_sms+'</td>';
                        html+= ' <td>'+data.sms_erro[em].total_sms_erro+'</td>';
                    }
                }
            }
        } else {
            for(ems=0;ems<data.sms.length;ems++){
                console.log(data.email[ems]);
                console.log(data.sms[ems]);
                html+= '<tr>';
                html+= ' <th></th>';
                html+= ' <td>Total EMAIL</td>';
                html+= ' <th>Total EMAIL com erro</th>';
                html+= ' <td>Total SMS</td>';
                html+= ' <td>Total SMS com erro</td>';
                html+= ' </tr>';
                html+= '<tr>';
                html += ' <td>'+data.email[ems].data_op+'</td>';
                for(em=0;em<data.sms.length;em++){
                    if(data.email_erro[em] !== undefined &&
                        data.email[ems].data_op === data.email_erro[em].data_op) {
                        html+= ' <td>'+data.email[ems].total_email+'</td>';
                        html+= ' <td>'+data.email_erro[em].total_email_erro+'</td>';
                    }
                }
                for(es=0;es<data.sms.length;es++){
                    if(data.sms_erro[es]!==undefined &&
                        data.sms[ems].data_op === data.sms_erro[es].data_op) {
                        html+= ' <td>'+data.sms[ems].total_sms+'</td>';
                        html+= ' <td>'+data.sms_erro[es].total_sms_erro+'</td>';
                    }
                }
            }
        }
        html+='</tbody>';
        $('#datatable').html(html);
        //return newData;
    };

    var montaTable = function(data){
        var html = '';
        html += '<tbody>';
        for(i=0;i<data.length;i++){
            html+= '<tr>';
            html+= ' <th></th>';
            html+= ' <td>Acesso Total</td>';
            html+= ' <td>Acessos Unicos</td>';
            html+= ' <td>Numero de Convertidos</td>';
            html+= ' </tr>';
            html+= '<tr>';
            html+= ' <th>'+data[i].data_archiv+'</th>';
            html+= ' <td>'+data[i].hits_totais+'</td>';
            html+= ' <td>'+data[i].hits_unicos+'</td>';
            html+= ' <td>'+data[i].total_conversao+'</td>';
            html+= ' </tr>';
        }
        html += '</tbody>';
        $('#datatable').html(html);
    };

    var promise = $.ajax({method: "get", url: 'http://contact.agentebrasil.com/contact/getStats/contact'});
    promise.done(function (resp) {
        montaLogicTable(resp);
        Highcharts.chart('container', {
            data: {
                table: 'datatable'
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Estatíticas de Contatos'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Units'
                }
            }
        });
    });
</script>
</body>
</html>