<html>
    <header>
        <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/dt-1.10.9/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/r/dt/dt-1.10.9/datatables.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </header>
    <body>
        <div class="">
            <div class="">
                <h1>Logs Servidor Contact</h1>
                <form id="formSearch">
                    <div class="form-group col-md-3">
                        <label for="">Campo</label>
                        <select name="" id="options" class="form-control">
                            <option value="contact_id">contact_id</option>
                            <option value="destinatario_fone">contact_id</option>
                            <option value="data_operacao">data_operacao</option>
                            <option value="destinatario_email">destinatario_email</option>
                            <option value="destinatario_nome">destinatario_nome</option>
                            <option value="email_status">email_status</option>
                            <option value="fl_finalizado">fl_finalizado</option>
                            <option value="lead_id">lead_id</option>
                            <option value="mensagem">mensagem</option>
                            <option value="produto_nome_email">produto_nome_email</option>
                            <option value="remetente_email">remetente_email</option>
                            <option value="remetente_nome">remetente_nome</option>
                            <option value="usuario_id">usuario_id</option>
                            <option value="canal_id">canal_id</option>
                            <option value="canal_id">data_finalizado</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="">Valor</label>
                        <input type="text" id="value" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label></label>
                        <button class="btn bg-primary" id="btnSearch" style="margin-top: 22px;">Pequisar</button>
                        <a style="margin-top: 22px;" href="http://contact.agentebrasil.com/contact/contact/stats" class="btn btn-primary">Estatísticas</a>
                    </div>
                    <div class="form-group col-md-3">
                        <button class="btn bg-primary" id="last100Registers" style="margin-top: 22px;">Listar Últimos 100 Registros de Email/SMS</button>
                    </div>
                    <div class="form-group col-md-12"></div>
                    <div class="form-group col-md-12">
                        <div id="infoStats"></div>
                    </div>
                    <div class="form-group col-md-12"></div>
                    <div class="form-group col-md-12"></div>
                </form>
                <div id="contr"></div>
            </div>
        </div>
    </body>
    <script>
        var url = 'http://contact.agentebrasil.com/contact/contact/viewStats';
        $(document).ready(function() {
            var promise = $.ajax({
                url: url,
                method:'post',
                headers: { 'Content-Type': 'application/json' }
            });
            promise.done(function(response){
                console.log(response);
                $('#contr').html(montaTable(response));
                $('#grid').DataTable({"order": [[ 0, "desc" ]]});
            });

//            var promiseStatus = $.ajax({
//                url: 'http://contact.agentebrasil.com/contact/getStats/contact',
//                method: 'get',
//                headers: { 'Content-Type': 'application/json' }
//            });
//            promiseStatus.done(function(response){
//                console.log(response);
//                montaInfo(response);
//            });
        });

        var montaInfo = function(data){
            var html = '';
            //listando sms
            for(i=0;i<data.sms.length;i++){
                html += 'Data: '+data.sms[i].data_op+' <br> Total de SMS:'+data.sms[i].total_sms+'<br>';
                for(k=0;k<data.sms_erro.length;k++){
                    if(data.sms[i].data_op == data.sms_erro[k].data_op){
                        html+= 'Total SMS com Falha: '+data.sms_erro[k].total_sms_erro+"<br>";
                    }
                }

                html+='--------------------------------<br>';
            }
            for(i=0;i<data.email.length;i++){
                html += 'Data: '+data.email[i].data_op+'<br> Total de EMAIL:'+data.email[i].total_email+'<br>';
                for(k=0;k<data.email_erro.length;k++){
                    if(data.email[i].data_op == data.email_erro[k].data_op){
                        html+= 'Total de Email com Falha: '+data.email_erro[k].total_email_erro+"<br>";
                    }
                }
                html+='--------------------------------<br>';
            }

            $('#infoStats').html(html);
        };
        //Botão pesquisar
        $('#formSearch').on('submit',function(e){
            e.preventDefault();
            var option = $('#options').val();
            var value = $('#value').val();
            if(value!="" && option != ""){
                var promise = $.ajax({
                    url: url,
                    method: 'post',
                    headers: { 'Content-Type': 'application/json' },
                    data: { option: option, value: value }
                });
                promise.done(function(response){
                    console.log(response);
                    $('#contr').html(montaTable(response));
                    $('#grid').DataTable({"order": [[ 0, "desc" ]]});
                });
            }
        });

        //Listar Últimos 100
        $('#last100Registers').on('click',function (e) {
            e.preventDefault();
            ///alert('teste');
            var order = 'desc';
            var limit = 100;
            if(order!="" && limit != ""){
                var promise = $.ajax({
                    url: url,
                    method: 'post',
                    headers: { 'Content-Type': 'application/json' },
                    data: { order: order, limit: limit }
                });
                promise.done(function(response){
                    console.log(response);
                    $('#contr').html(montaTable(response));
                    $('#grid').DataTable({"order": [[ 0, "desc" ]]});
                });
            }
        });
        var montaTable = function(data){
            html = '';
            html += '<table id="grid" class="display" width="100%" cellspacing="0">';
            html += '<thead>';
            html += '<tr>'
            html += '<th>Contact ID</th>';
            html += '<th>Destinatario Fone</th>';
            html += '<th>Data Operação</th>';
            html += '<th>Destinatário Email</th>';
            html += '<th>Destinatário Nome</th>';
            html += '<th>Status</th>';
            html += '<th>Finalizado</th>';
            html += '<th>LeadID</th>';
            html += '<th>Mensagem</th>';
            html += '<th>Nome do Produto</th>';
            html += '<th>Remetente Email</th>';
            html += '<th>Remetente Nome</th>';
            html += '<th>Usuario ID</th>';
            html += '<th>TIPO DE CANAL</th>';
            html += '<th>Data de Finalização</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            for(i=0;i<data.length;i++){
                html+= '<tr>';
                html+= '<td>'+data[i].contact_id+'</td>';
                html+= '<td>'+data[i].destinatario_fone+'</td>';
                html+= '<td>'+data[i].data_operacao+'</td>';
                html+= '<td>'+data[i].destinatario_email+'</td>';
                html+= '<td>'+data[i].destinatario_nome+'</td>';
                html+= '<td>'+data[i].email_status+'</td>';
                html+= '<td>'+data[i].fl_finalizado+'</td>';
                html+= '<td>'+data[i].lead_id+'</td>';
                html+= '<td>'+data[i].mensagem+'</td>';
                html+= '<td>'+data[i].produto_nome_email+'</td>';
                html+= '<td>'+data[i].remetente_email+'</td>';
                html+= '<td>'+data[i].remetente_nome+'</td>';
                html+= '<td>'+data[i].usuario_id+'</td>';
                html+= '<td>'+data[i].canal_id+'</td>';
                html+= '<td>'+data[i].data_finalizado+'</td>';
                html+= '</tr>';
            }
            html += '</tbody>';
            html += '<tfoot>';
            html += '<tr>';
            html += '<th>Contact ID</th>';
            html += '<th>Destinatario Fone</th>';
            html += '<th>Data Operação</th>';
            html += '<th>Destinatário Email</th>';
            html += '<th>Destinatário Nome</th>';
            html += '<th>Status</th>';
            html += '<th>Finalizado</th>';
            html += '<th>LeadID</th>';
            html += '<th>Mensagem</th>';
            html += '<th>Nome do Produto</th>';
            html += '<th>Remetente Email</th>';
            html += '<th>Remetente Nome</th>';
            html += '<th>Usuario ID</th>';
            html += '<th>TIPO DE CANAL</th>';
            html += '<th>Data de Finalização</th>';
            html += '</tr>';
            html += '</tfoot>';
            return html;
        }
    </script>
</html>