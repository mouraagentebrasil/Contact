<?php
/**
 * Created by PhpStorm.
 * User: felipemoura
 * Date: 09/12/2016
 * Time: 10:25
 */

namespace Modules\Contact\Http\Controllers;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;
use Mailgun\Mailgun;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Contact\Entities\Contact;

class SendContactController extends Controller
{

    protected $client = null;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function sendMailCobranca($data,$contact_id)
    {
        $removeItem = function($item){
            $new = str_replace('<','',$item);
            $new = str_replace('>','',$new);
            return $new;
        };
        try {
            $response = Mail::send('contact::email_generic',['data'=>$data],function($m) use ($data){
                $m->from($data['remetente_email'], $data['remetente_nome'].' - '.$data['produto_nome_email']);
                $m->to($data['destinatario_email'], $data['destinatario_nome'])->subject('Lembrete de cobrança');
            });
            $objQeue = json_decode($response->getBody()->getContents());
            $mailgun_id = $removeItem($objQeue->id);
            Contact::find($contact_id)->update(['mailgun_id'=>$mailgun_id,'retorno_callback_envio'=>json_encode($objQeue)]);
        } catch (\Exception $e){
            Contact::find($contact_id)->update(['retorno_callback_envio'=>$e->getMessage().'\n'.$e->getFile()."\n".$e->getLine()]);
        }
    }

    public function sendMailCobrancaBradesco($data,$contact_id)
    {
        $removeItem = function($item){
            $new = str_replace('<','',$item);
            $new = str_replace('>','',$new);
            return $new;
        };
        try {
            $response = Mail::send('contact::email_generic_bradesco',['data'=>$data],function($m) use ($data){
                $m->from($data['remetente_email'], $data['remetente_nome'].' - '.$data['produto_nome_email']);
                $m->to($data['destinatario_email'], $data['destinatario_nome'])->subject('Lembrete de cobrança');
            });
            $objQeue = json_decode($response->getBody()->getContents());
            $mailgun_id = $removeItem($objQeue->id);
            Contact::find($contact_id)->update(['mailgun_id'=>$mailgun_id,'retorno_callback_envio'=>json_encode($objQeue)]);
        } catch (\Exception $e){
            Contact::find($contact_id)->update(['retorno_callback_envio'=>$e->getMessage().'\n'.$e->getFile()."\n".$e->getLine()]);
        }
    }

    public function sendMail($data,$contact_id)
    {
        $removeItem = function($item){
            $new = str_replace('<','',$item);
            $new = str_replace('>','',$new);
            return $new;
        };
        try {
            $response = Mail::send('contact::email_generic_empty',['data'=>$data],function($m) use ($data){
                $m->from($data['remetente_email'], $data['remetente_nome'].' - '.$data['produto_nome_email']);
                $m->to($data['destinatario_email'], $data['destinatario_nome'])->subject('Lembrete de cobrança');
            });
            $objQeue = json_decode($response->getBody()->getContents());
            //var_dump($objQeue); exit;
            $mailgun_id = $removeItem($objQeue->id);
            Contact::find($contact_id)->update(['mailgun_id'=>$mailgun_id,'retorno_callback_envio'=>json_encode($objQeue)]);
        } catch (\Exception $e){
            Contact::find($contact_id)->update(['retorno_callback_envio'=>$e->getMessage().'\n'.$e->getFile()."\n".$e->getLine()]);
        }
    }

    public function checkStateEmail($message_id = null)
    {
        try {
            $url = "https://api.mailgun.net/v3/".getenv('MAILGUN_DOMAIN')."/events";
            $r = $this->client->request('GET',$url,[
                'auth'=>['api',getenv('MAILGUN_SECRET')],
                'query'=>[
                    'ascending'=>'no',
                    'limit'=>'25',
                    'message-id'=>$message_id
                ]
            ]);
            return $r->getBody()->getContents();
        } catch (\Exception $e){
            return $e->getMessage().'\n'.$e->getFile().'\n'.$e->getLine();
        }
    }

    public function checkStateSms($message_id){
        try {
            $r = $this->client->request('GET', getenv('ZENVIA_ENDPOINT').'/get-sms-status/'.$message_id, [
                'headers' => [
                    'Authorization' => 'Basic ' . getenv('ZENVIA_TOKEN'),
                    //'Content-Type' => 'application/json'
                    'Accept' => 'application/json'
                ],
            ]);
            return $r->getBody()->getContents();
        } catch(\Exception $e){
            return $e->getMessage().'\n'.$e->getFile().'\n'.$e->getLine();
        }
    }

    public function sendSms($data,$contact_id)
    {
        $data_send = [];
        $id = $data['lead_id'] . "000" . time();
        $data_send['sendSmsRequest']['from'] = $data['remetente_nome'];
        $data_send['sendSmsRequest']['to'] = $data['destinatario_fone'];
        $data_send['sendSmsRequest']['schedule'] = date('Y-m-d') . 'T' . date('H:i:s');
        $data_send['sendSmsRequest']['msg'] = $data['mensagem'];
        $data_send['sendSmsRequest']['id'] = $id;
        $data_send['sendSmsRequest']['aggregateId'] = $data['lead_id'];
        try {
            $r = $this->client->request('POST', getenv('ZENVIA_ENDPOINT').'/send-sms', [
                'headers' => [
                    'Authorization' => 'Basic ' . getenv('ZENVIA_TOKEN'),
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
                'body' => json_encode($data_send)
            ]);
            $response = json_decode($r->getBody()->getContents());
            //var_dump($r->getStatusCode()); exit;
            $fl_finalizado = $response->sendSmsResponse->statusDescription == "Error" ? 1 : 0;
            $email_status = $fl_finalizado ? "Failed" : "Queued";
            Contact::find($contact_id)->update([
                'retorno_callback_envio' => json_encode($data_send),
                'retorno_callback_recebimento' => json_encode($response),
                'mailgun_id' => $id,
                'fl_finalizado'=>$fl_finalizado,
                'email_status'=>$email_status
            ]);
        } catch(\Exception $e){
            Contact::find($contact_id)->update([
                'retorno_callback_envio'=>$e->getMessage().'\n'.$e->getFile()."\n".$e->getLine()
            ]);
        }
    }
}