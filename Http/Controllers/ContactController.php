<?php

namespace Modules\Contact\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Contact\Entities\Contact;
use Modules\Contact\Repositories\ContactRepository;

class ContactController extends Controller
{
    
    //retorna item paginados
    public function index()
    {
        $repository = new ContactRepository;
        return $repository->builder()->paginate();
    }

    //metodo para salvar um novo registro
    public function storeMailCobrancaBradesco(Request $request)
    {
        $data = $request->all();
        $repository = new ContactRepository;
        $data['data_operacao'] = date('Y-m-d H:i:s');

        $data['canal_id'] = 1;
        $data['remetente_email'] = 'comunica@unimedab.com.br';
        $data['remetente_nome'] = 'AgenteBrasil';
        $data['produto_nome_email'] = 'Unimed Odonto';

        $validate = $repository->builder()->validate($data,[
            'campanha_id','custo','device_key','retorno_callback_envio','fl_finalizado',
            'retorno_callback_recebimento','usuario_id','mailgun_id','email_status','data_finalizado',
            'destinatario_fone','mensagem','fl_visualizado','data_visualizado','number_hits','data_ultima_visualizacao'
        ]);

        if(is_array($validate)) return response()->json($validate,400);

        $save = $repository->store($data,'*');
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        $send = (new SendContactController())->sendMailCobrancaBradesco($data,$save);

        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //metodo para salvar um novo registro
    public function storeMailCobranca(Request $request)
    {
        $data = $request->all();
        $repository = new ContactRepository;
        $data['data_operacao'] = date('Y-m-d H:i:s');

        $data['canal_id'] = 1;
        $data['remetente_email'] = 'comunica@unimedab.com.br';
        $data['remetente_nome'] = 'AgenteBrasil';
        $data['produto_nome_email'] = 'Unimed Odonto';

        $validate = $repository->builder()->validate($data,[
            'campanha_id','custo','device_key','retorno_callback_envio','fl_finalizado',
            'retorno_callback_recebimento','usuario_id','mailgun_id','email_status','data_finalizado',
            'destinatario_fone','mensagem','cobranca','fl_visualizado','data_visualizado','number_hits','data_ultima_visualizacao'
        ]);

        if(is_array($validate)) return response()->json($validate,400);

        $save = $repository->store($data,'*');
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        $send = (new SendContactController())->sendMailCobranca($data,$save);

        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {
        $data = $request->all();
        $repository = new ContactRepository;
        $data['data_operacao'] = date('Y-m-d H:i:s');
        $sendType = '';
        if(!empty($data['canal_id']) && $data['canal_id']==1){
           $validate = $repository->builder()->validate($data,[
                'campanha_id','custo','device_key','retorno_callback_envio','fl_finalizado',
                'retorno_callback_recebimento','usuario_id','mailgun_id','email_status','data_finalizado',
                'cobranca','fl_visualizado','data_visualizado','number_hits','data_ultima_visualizacao'
            ]);
            $sendType = 'sendMail';
        } else {
            $validate = $repository->builder()->validate($data,[
                'campanha_id','custo','device_key','retorno_callback_envio','fl_finalizado',
                'retorno_callback_recebimento','usuario_id','mailgun_id','email_status','destinatario_email',
                'remetente_email','data_finalizado','cobranca','fl_visualizado','data_visualizado','number_hits','data_ultima_visualizacao'
            ]);
            $sendType = 'sendSms';
        }
        if(is_array($validate)) return response()->json($validate,400);

        $save = $repository->store($data,'*');
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        $send = (new SendContactController())->$sendType($data,$save);

        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new ContactRepository;
        $update = $repository->update($id,$data);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new ContactRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }
    //REFATORAR URGENTE
    public function viewStats(Request $request)
    {
        $data = $request->all();
        $repository = new ContactRepository;
        $builder = $repository->builder()->select([
            'contact_id','destinatario_fone','data_operacao','destinatario_email',
            'destinatario_fone','destinatario_nome','email_status',
            'lead_id','mensagem','produto_nome_email','remetente_email','remetente_nome',
            'usuario_id', 'data_finalizado','data_visualizado','number_hits','data_ultima_visualizacao'
        ]);

        $builder->selectRaw("case when fl_finalizado = 1 THEN 'Finalizado' else 'Não Finalizado' end as fl_finalizado");
        $builder->selectRaw("case when canal_id = 1 THEN 'EMAIL' else 'SMS' end as canal_id");
        $builder->selectRaw("case when fl_visualizado = 1 THEN 'Visualizado' else 'Não Vizualizado' end as fl_visualizado");

        $builder2 = $repository->builder()->select([
            'contact_id','destinatario_fone','data_operacao','destinatario_email',
            'destinatario_fone','destinatario_nome','email_status',
            'lead_id','mensagem','produto_nome_email','remetente_email','remetente_nome',
            'usuario_id', 'data_finalizado','fl_visualizado','data_visualizado','number_hits','data_ultima_visualizacao'
        ]);

        $builder2->selectRaw("case when fl_finalizado = 1 THEN 'Finalizado' else 'Não Finalizado' end as fl_finalizado");
        $builder2->selectRaw("case when canal_id = 1 THEN 'EMAIL' else 'SMS' end as canal_id");
        $builder2->selectRaw("case when fl_visualizado = 1 THEN 'Visualizado' else 'Não Vizualizado' end as fl_visualizado");

        if(!empty($data['option'])){
            $builder->where($data['option'],'like','%'.trim($data['value']).'%');
            $builder->orderby('contact_id','desc');
        } elseif(!empty($data['order'])){
            $cons1 = $builder->where('canal_id',1)->orderby('contact_id','desc')->limit($data['limit'])->get()->toArray();
            $cons2 = $builder2->where('canal_id',2)->orderby('contact_id','desc')->limit($data['limit'])->get()->toArray();
            $return = [];
            $i=0;
            foreach ($cons1 as $item) {
                $return[$i] = $item;
                $i++;
            }
            foreach ($cons2 as $item) {
                $return[$i] = $item;
                $i++;
            }
            return $return;
        } else {
            $builder->orderby('contact_id','desc');
            $builder->limit(100);
        }


        return $builder->get();
    }

    public function getTotaisStatsContact(){
        $findSms = Contact::selectRaw('count(*) as total_sms, date_format(data_operacao,"%Y-%m-%d") as data_op')
            ->where('canal_id',2)
            ->groupBy('data_op')
            ->get();

        $findFailedSMS = Contact::selectRaw('count(*) as total_sms_erro, date_format(data_operacao,"%Y-%m-%d") as data_op')
            ->whereIn('email_status',['Failed','Not Received'])
            ->where('canal_id',2)
            ->groupBy('data_op')
            ->get();

        $findEmail = Contact::selectRaw('count(*) as total_email, date_format(data_operacao,"%Y-%m-%d") as data_op')
            ->where('canal_id',1)
            ->groupBy('data_op')
            ->get();

        $findFailedEmail = Contact::selectRaw('count(*) as total_email_erro, date_format(data_operacao,"%Y-%m-%d") as data_op')
            ->whereIn('email_status',['Failed','Not Received'])
            ->where('canal_id',1)
            ->groupBy('data_op')
            ->get();

        $return['sms'] = $findSms;
        $return['sms_erro'] = $findFailedSMS;
        $return['email'] = $findEmail;
        $return['email_erro'] = $findFailedEmail;

        return $return;
    }

    public function setCobranca($cobranca)
    {
        $contacts = Contact::where('cobranca',$cobranca)->get();
        if(empty($contacts)) return response()->json(['message'=>'Cobranca não encontrada!'],400);
        foreach ($contacts as $contact){
            if(!is_null($contact->data_visualizado)){
                $update = Contact::where('cobranca',$cobranca)->update([
                    'fl_visualizado'=>1,
                    'data_ultima_visualizacao'=>date('Y-m-d H:i:s')
                ]);
            } else {
                $update = Contact::where('cobranca',$cobranca)->update([
                    'fl_visualizado'=>1,
                    'data_visualizado'=>date('Y-m-d H:i:s'),
                    'data_ultima_visualizacao'=>date('Y-m-d H:i:s')
                ]);
            }
        }
        $increment = Contact::where('cobranca',$cobranca)->increment('number_hits');
        return response()->json(['message'=>'Contact cobranca atualizada'],200);
    }
}