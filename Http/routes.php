<?php

Route::group(['middleware' => ['web','cors'], 'prefix' => 'contact', 'namespace' => 'Modules\Contact\Http\Controllers'], function()
{
    //Route::get('/', 'ContactController@index');
    //Rotas para canal
    Route::get('/canal', 'CanalController@index');
    Route::post('/canal/store', 'CanalController@store');
    Route::post('/canal/update/{id}','CanalController@update');
    Route::get('/canal/destroy/{id}','CanalController@destroy');

    
    //Rotas para contact
    Route::get('/contact', 'ContactController@index');
    Route::post('/contact/store', 'ContactController@store');
    Route::post('/contact/update/{id}','ContactController@update');
    Route::get('/contact/destroy/{id}','ContactController@destroy');
    Route::get('/checkEmail/{id?}','SendContactController@checkStateEmail');
    Route::get('/contact/checkEmail/{id?}','SendContactController@checkStateEmail');
    Route::get('/checkSms/{id}','SendContactController@checkStateSms');
    Route::any('/contact/viewStats','ContactController@viewStats');
    Route::post('/legado/storeMailCobranca','ContactController@storeMailCobranca');
    Route::post('/legado/storeMailCobrancaBradesco','ContactController@storeMailCobrancaBradesco');
    Route::get('/getStats/contact','ContactController@getTotaisStatsContact');
    Route::get('/setCob/{cobranca}','ContactController@setCobranca');
    Route::get('/contact/logs_contato',function(){
        return view('contact::viewStats');
    });
    Route::get('/contact/stats',function(){
        return view('contact::statsGraph');
    });
    //[CODE]
});
