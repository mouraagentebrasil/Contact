<?php

namespace Modules\Contact\Console;

use Illuminate\Console\Command;
use Modules\Contact\Http\Controllers\SendContactController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Contact\Entities\Contact;

class AtualizaContactMailgunCommand extends Command
{
    protected $send;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'update_status_contact';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->send = new SendContactController();
    }
    public function consoleEmail(){
        $allContacts = Contact::where('fl_finalizado',0)->where('canal_id',1)->get()->toArray();
        $c = 0;
        foreach($allContacts as $contact) {
            $data = json_decode($this->send->checkStateEmail($contact['mailgun_id']));

            //var_dump(@$data->items[0]->event);
            if(empty($data->items)){
                $c++;
                echo ($contact['mailgun_id'])." - ".$c.PHP_EOL;
                continue;
            }

            $status = ucfirst($data->items[0]->event);
            $finalizado = in_array($status,["Delivered","Failed"]) ? "1" : "0";
            $date_finalizado = null;
            if($finalizado=="1"){
                $date_finalizado = date('Y-m-d H:i:s');
            }
            Contact::find($contact['contact_id'])->update([
                'retorno_callback_recebimento'=>json_encode($data->items[0]),
                'email_status'=>$status,
                'fl_finalizado'=>$finalizado,
                'data_finalizado'=>$date_finalizado
            ]);
        }
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->consoleEmail();
        $this->consoleSms();
    }

    public function consoleSms(){
        $allContacts = Contact::where('fl_finalizado',0)->where('canal_id',2)->get()->toArray();
        foreach($allContacts as $contact) {
            $data = json_decode($this->send->checkStateSms($contact['mailgun_id']));
            $status = $data->getSmsStatusResp->statusDescription;
            $finalizado = in_array($status,["Delivered","Failed","Error","Not Received"]) ? "1" : "0";
            $date_finalized = null;
            if($finalizado==1){
                $date_finalized = $data->getSmsStatusResp->received;
                if($date_finalized!=null){
                    $date_finalized  = str_replace('T',' ',$date_finalized);
                } else {
                    $date_finalized = date('Y-m-d');
                }
            }

            Contact::find($contact['contact_id'])->update([
                'retorno_callback_recebimento'=>json_encode($data->getSmsStatusResp),
                'email_status'=>$status,
                'fl_finalizado'=>$finalizado,
                'data_finalizado'=>$date_finalized
            ]);
        }
    }
}
