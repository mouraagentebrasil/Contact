<?php

namespace Modules\Contact\Entities;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table='contact';
    protected $primaryKey='contact_id';
    protected $fillable=[
        'contact_id','destinatario_fone','data_operacao','lead_id','mensagem',
        'campanha_id','device_key','custo','retorno_callback_envio','retorno_callback_recebimento',
        'usuario_id','canal_id','destinatario_email','destinatario_nome','produto_nome_email',
        'remetente_email','remetente_nome','mailgun_id','email_status','fl_finalizado',
        'data_finalizado','cobranca','fl_visualizado','data_visualizado','number_hits','data_ultima_visualizacao'
    ];
    public $timestamps = false;

    
//
//    public function Lead()
//    {
//        return $this->belongsTo('\Modules\Contact\Entities\Lead');
//    }
//
//    public function Campanha()
//    {
//        return $this->belongsTo('\Modules\Contact\Entities\Campanha');
//    }
//
//    public function Usuario()
//    {
//        return $this->belongsTo('\Modules\Contact\Entities\Usuario');
//    }

    public function Canal()
    {
        return $this->belongsTo('\Modules\Contact\Entities\Canal');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
